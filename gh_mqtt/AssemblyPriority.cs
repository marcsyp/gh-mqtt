﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Grasshopper;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Special;
using System.Net.NetworkInformation;

using MQTTnet;
using MQTTnet.Packets;

namespace gh_mqtt
{
    public class AssemblyPriority : GH_AssemblyPriority
    {
        //bool auth = false;
        


        public override GH_LoadingInstruction PriorityLoad()
        {
            // Check for internet
            if (MQTT_Util.CheckForInternet())
            {

                MQTT_Util.networkAvailable = true;
                MQTT_Util.setIPaddress();

                // Create a new MQTT client.
                MQTT_Util.CreateClient();


                if (MQTT_Util.mqttClient == null)
                {
                    // Create a new MQTT client.
                    MqttFactory factory = new MqttFactory();
                    MQTT_Util.mqttClient = factory.CreateMqttClient();
                }


            }

            // Add the handlers to the NetworkChange events.
            NetworkChange.NetworkAvailabilityChanged -= NetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged -= NetworkAddressChanged;
            NetworkChange.NetworkAvailabilityChanged += NetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged += NetworkAddressChanged;

            //Console.ReadLine();


            //    if (Squirrel_Util.Authenticate())
            //    {
            //        if (Squirrel_Util.TrackLoadEvent())
            //        {
            //            auth = true;

            //        }
            //    }
            return GH_LoadingInstruction.Proceed;
        }

        // Declare a method to handle NetworkAvailabilityChanged events.
        private static void NetworkAvailabilityChanged(
        object sender, NetworkAvailabilityEventArgs e)
        {
            // Report whether the network is now available or unavailable.
            if (e.IsAvailable)
            {
                MQTT_Util.networkAvailable = true;
                MQTT_Util.setIPaddress();
                //Squirrel_Util.sendQueuedEvents();
            }
            else
            {
                MQTT_Util.networkAvailable = false;
            }
        }

        // Declare a method to handle NetworkAdressChanged events.
        private static void NetworkAddressChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Current IP Addresses:");
            // Iterate through the interfaces and display information.
            foreach (NetworkInterface ni in
            NetworkInterface.GetAllNetworkInterfaces())
            {
                MQTT_Util.setIPaddress();

                //List<System.Net.IPAddress> ipAddresses = ni.GetIPProperties().UnicastAddresses.Select(a => a.Address) as List<System.Net.IPAddress>;
                //Squirrel_Util.ipAddresses = ipAddresses;

                //foreach (UnicastIPAddressInformation addr
                //in ni.GetIPProperties().UnicastAddresses)
                //{

                //    Console.WriteLine(" - {0} (lease expires {1})",
                //    addr.Address, DateTime.Now +
                //    new TimeSpan(0, 0, (int)addr.DhcpLeaseLifetime));
                //}
            }
        }
    }
}