﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Grasshopper;
using Grasshopper.Kernel;
using Rhino.Geometry;


using MQTTnet;
using MQTTnet.Packets;

namespace gh_mqtt.Components
{
    public class ConnectToBroker : GH_Component
    {

        private string lastURI = "";

        private Object mutex = new Object();
        private bool status = false;
        private Uri uri;
        private bool nowStreaming = false;

        /// <summary>
        /// Initializes a new instance of the ConnectToBroker class.
        /// </summary>
        public ConnectToBroker()
          : base("ConnectToBroker", "Broker",
              "Connect to an MQTT broker",
              "MQTT", "Connect")
        {
        }

        public override void AddedToDocument(GH_Document document)
        {
            // register callback
            client.Disconnected -= ClientDisconnected;
            client.Disconnected += ClientDisconnected;
            client.Connected -= ClientConnected;
            client.Connected += ClientConnected;
            base.AddedToDocument(document);
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            // add input parameters
            pManager.AddTextParameter("Broker URI", "B", "The URI of the broker", GH_ParamAccess.item, "");
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add output parameters
            pManager.AddBooleanParameter("Status", "S", "Status report", GH_ParamAccess.item);
        }

        MQTTnet.Client.IMqttClient client = MQTT_Util.mqttClient;

        protected override void SolveInstance(IGH_DataAccess DA)
        {


            // set output value
            lock (mutex)
            {
                DA.SetData(0, status);
            }

            // get data
            String rawURI = "";

            if (!DA.GetData(0, ref rawURI)) return;

            // return if uri has not changed
            if (lastURI != rawURI)
            {
                // validate uri
                if (rawURI == "")
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Missing URI");
                    return;
                }

                // This will run each time a new input is provided and prevent double disconnection in the eventHandler
                nowStreaming = false;

                try
                {
                    // parse uri
                    uri = new Uri(rawURI);
                }
                catch (Exception err)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Uri error:" + err.ToString());
                    return;
                }
                

                // disconnect current client (ignoring errors)
                if (client != null)
                {
                    try
                    {
                        client.DisconnectAsync();
                    }
                    catch (Exception err)
                    {
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Connection error:" + err.ToString());
                        return;
                    }

                    client = null;
                }

                // get port
                int port = uri.Port;
                if (port <= 0)
                {
                    port = 1883;
                }


                //// create client instance  // created in AssemblyPriority
                //MQTT_Util.CreateClient();


                if (MQTT_Util.mqttClient == null)
                {
                    MQTT_Util.CreateClient();
                }

                client = MQTT_Util.mqttClient;



                // default to # if path is empty
                var path = uri.AbsolutePath;
                if (path == "")
                {
                    path = "#";
                }

                // get username password
                var creds = uri.UserInfo.Split(':');
                MQTTnet.Client.IMqttClientOptions options = new MQTTnet.Client.MqttClientOptionsBuilder()
                    .Build();
                // connect to broker
                if (creds.Length == 0)
                {
                    options = new MQTTnet.Client.MqttClientOptionsBuilder()
                    .WithTcpServer("broker.hivemq.com", 1883) // Port is optional
                    .Build();
                    //client.ConnectAsync("");
                }
                else if (creds.Length == 1)
                {
                    string server = uri.Host;
                    options = new MQTTnet.Client.MqttClientOptionsBuilder()
                    .WithTcpServer(server, port) // Port is optional
                    .Build();
                    //client.Connect("", creds[0], "");
                }
                else if (creds.Length == 2)
                {
                    //client.Connect("", creds[0], creds[1]);
                }

                if (client != null && !client.IsConnected && !nowStreaming)
                {
                    try
                    {
                        client.ConnectAsync(options);
                        nowStreaming = true;

                    } catch (Exception err) {
                        AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Connection error:" + err.ToString());
                        return;
                    }
                }

                // set last uri
                lastURI = rawURI;
            } else
            {
                return;
            }
            

        }

        private void ClientConnected (object sender, MQTTnet.Client.MqttClientConnectedEventArgs e)
            {
                // Set Status
                status = client.IsConnected;
                // Expire component here to update???
                    
            }

    private void ClientDisconnected(object sender, MQTTnet.Client.MqttClientDisconnectedEventArgs e)
        {
            status = client.IsConnected;
            //clientConnected = false;
            if (MQTT_Util.mqttClient == null && nowStreaming)  // check to ensure that this is a flaky disconnection, not a GH disconnection
            {
                MQTT_Util.CreateClient();
            }

            client = MQTT_Util.mqttClient;

            // get port
            int port = uri.Port;
            if (port <= 0)
            {
                port = 1883;
            }

            // default to # if path is empty
            var path = uri.AbsolutePath;
            if (path == "")
            {
                path = "#";
            }

            // get username password
            var creds = uri.UserInfo.Split(':');
            MQTTnet.Client.IMqttClientOptions options = new MQTTnet.Client.MqttClientOptionsBuilder()
                .Build();
            // connect to broker
            if (creds.Length == 0)
            {
                options = new MQTTnet.Client.MqttClientOptionsBuilder()
                .WithTcpServer("broker.hivemq.com", 1883) // Port is optional
                .Build();
                //client.ConnectAsync("");
            }
            else if (creds.Length == 1)
            {
                string server = uri.Host;
                options = new MQTTnet.Client.MqttClientOptionsBuilder()
                .WithTcpServer(server, port) // Port is optional
                .Build();
                //client.Connect("", creds[0], "");
            }
            else if (creds.Length == 2)
            {
                //client.Connect("", creds[0], creds[1]);
            }

            if (client != null && !client.IsConnected)
            {
                try
                {
                    client.ConnectAsync(options);
                    nowStreaming = true;

                }
                catch (Exception err)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Connection error:" + err.ToString());
                    return;
                }

            }
        }

        private void SolutionCallback(GH_Document document)
        {
            this.ExpireSolution(false); // `false` because a new solution is already in the works.
        }

        //bool _askingNewSolution = false;

        
        public override void RemovedFromDocument(GH_Document document)
        {
            // disconenct client (ignoring errors)
            if (client != null)
            {
                try
                {
                    client.DisconnectAsync();
                    client.Disconnected -= ClientDisconnected;
                    client.Connected -= ClientConnected;

                }
                catch (Exception err) {
                    
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Disconnection error:" + err.ToString());
                    return;
                    
                }
            }

            base.RemovedFromDocument(document);
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("6477bed0-06ec-418d-9a57-fc4b50edd9c3"); }
        }
    }
}