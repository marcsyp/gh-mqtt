﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Grasshopper;
using Grasshopper.Kernel;
using Rhino.Geometry;


using MQTTnet;
using MQTTnet.Packets;


namespace gh_mqtt.Components
{
    public class MQTT_Receiver : GH_Component
    {
        private string lastTopic = "";

        private Object mutex = new Object();
        private string receivedTopic = "";
        private string payload = "";

        public MQTT_Receiver()
          : base("MQTT Receiver", "MQTT Receiver",
              "Receive messages from an MQTT client.",
              "MQTT", "IO")
        {
        }


        public override void AddedToDocument(GH_Document document)
        {
            // register callback
            client.ApplicationMessageReceived -= MessageReceived;
            client.ApplicationMessageReceived += MessageReceived;
            base.AddedToDocument(document);
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            // add input parameters
            pManager.AddTextParameter("Topic", "T", "The topic for subscription", GH_ParamAccess.item, "");
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add output parameters
            pManager.AddTextParameter("Topic", "T", "The topic", GH_ParamAccess.item);
            pManager.AddTextParameter("Payload", "P", "The payload", GH_ParamAccess.item);
        }

        MQTTnet.Client.IMqttClient client = MQTT_Util.mqttClient;

        protected override void SolveInstance(IGH_DataAccess DA)
        {

            string topic = "";
            // set output value
            lock (mutex)
            {
                DA.SetData(0, receivedTopic);
                DA.SetData(1, payload);
            }


            // Unsubscribe from previous topic
            if (client.IsConnected)
            {

                // get data
                if (!DA.GetData(0, ref topic)) return;

                // return if topic has not changed
                if (lastTopic == topic)
                {
                    return;
                }

                lastTopic = topic;


                List<string> tString = new List<string>();
                tString.Add(lastTopic);
                client.UnsubscribeAsync(tString);
            }

            if (client.IsConnected && topic != "")
            {
                List<TopicFilter> tFilter = new List<TopicFilter>();
                tFilter.Add(new TopicFilterBuilder().WithTopic(topic).Build());
                
                try
                {
                    client.SubscribeAsync(tFilter);

                } catch (Exception err) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Subscribe error: " + err.ToString());
                    return;
                }

            };
        }
        
        private void SolutionCallback(GH_Document document)
        {
            this.ExpireSolution(false); // `false` because a new solution is already in the works.
        }

        bool _askingNewSolution = false;

        private void MessageReceived(object sender, MQTTnet.MqttApplicationMessageReceivedEventArgs e)
        {
            // update topic and payload
            lock (mutex)
            {
                receivedTopic = e.ApplicationMessage.Topic;
                payload = System.Text.Encoding.UTF8.GetString(e.ApplicationMessage.Payload);

                //Console.WriteLine($"+ Topic = {e.ApplicationMessage.Topic}");
                //Console.WriteLine($"+ Payload = {System.Text.Encoding.UTF8.GetString(e.ApplicationMessage.Payload)}");
                //Console.WriteLine($"+ QoS = {e.ApplicationMessage.QualityOfServiceLevel}");
                //Console.WriteLine($"+ Retain = {e.ApplicationMessage.Retain}");

            }

            try
            {
                // ripped/modded from gHowl

                GH_Document _doc = OnPingDocument();
                if (_doc.SolutionState != GH_ProcessStep.Process && client != null && !_askingNewSolution)
                {
                    ((Control)Grasshopper.Instances.DocumentEditor).BeginInvoke((Action)delegate () {
                        if (_doc.SolutionState != GH_ProcessStep.Process)
                        {
                            _askingNewSolution = true;
                            this.ExpireSolution(true);
                            _askingNewSolution = false;
                        }
                    });
                }

                

            }
            catch (Exception err)
            {
                AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Expire Solution error: " + err.ToString());
                return;
            }

        }

        public override void RemovedFromDocument(GH_Document document)
        {
            // disconenct client (ignoring errors)
            if (client != null)
            {
                try
                {
                    client.ApplicationMessageReceived -= MessageReceived;
                    client.DisconnectAsync();
                    // REMOVE SUBSCRIPTIONS FROM CENTRAL REGISTRY HERE

                }
                catch (Exception err) {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "RemovedFromDocument error: " + err.ToString());
                    return;
                }
            }

            base.RemovedFromDocument(document);
        }

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }

        /// <summary>
        /// Each component must have a unique Guid to identify it. 
        /// It is vital this Guid doesn't change otherwise old ghx files 
        /// that use the old ID will partially fail during loading.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("4f9f0d20-0615-453f-babd-053f41768873"); }
        }
    }
}
