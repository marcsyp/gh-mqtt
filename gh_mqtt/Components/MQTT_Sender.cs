﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

using Grasshopper;
using Grasshopper.Kernel;
using Rhino.Geometry;


using MQTTnet;
using MQTTnet.Packets;

namespace gh_mqtt.Components
{
    public class MQTT_Sender : GH_Component
    {

        private Object mutex = new Object();
        private string topic = "";
        private string payload = "";

        /// <summary>
        /// Initializes a new instance of the MQTT_Sender class.
        /// </summary>
        public MQTT_Sender()
          : base("MQTT_Sender", "MQTT Send",
              "Send messages via MQTT.",
              "MQTT", "IO")
        {
        }

        public override void AddedToDocument(GH_Document document)
        {
            // register callback
            //client.ApplicationMessageReceived -= MessageReceived;
            //client.ApplicationMessageReceived += MessageReceived;
            //client.Disconnected -= ClientDisconnected;
            //client.Disconnected += ClientDisconnected;
            base.AddedToDocument(document);
        }

        protected override void RegisterInputParams(GH_Component.GH_InputParamManager pManager)
        {
            // add input parameters
            //pManager.AddTextParameter("Broker URI", "B", "The URI of the broker", GH_ParamAccess.item, "");
            pManager.AddTextParameter("Topic", "T", "The topic", GH_ParamAccess.item);
            pManager.AddTextParameter("Payload", "P", "The payload", GH_ParamAccess.item);
        }

        protected override void RegisterOutputParams(GH_Component.GH_OutputParamManager pManager)
        {
            // add output parameters
            pManager.AddTextParameter("Status", "S", "Status report", GH_ParamAccess.item);
        }

        MQTTnet.Client.IMqttClient client = MQTT_Util.mqttClient;

        protected override async void SolveInstance(IGH_DataAccess DA)
        {


            // get data
            //String topic = "";
            //String payload = "";

            if (!DA.GetData(0, ref topic)) return;
            if (!DA.GetData(1, ref payload)) return;


            if (client.IsConnected)
            {
                // SEND MESSAGE

                var message = new MqttApplicationMessageBuilder()
                    .WithTopic(topic)
                    .WithPayload(payload)
                    //.WithExactlyOnceQoS()
                    //.WithRetainFlag()
                    .Build();
                try
                {
                    await client.PublishAsync(message);
                } catch (Exception err)
                {
                    AddRuntimeMessage(GH_RuntimeMessageLevel.Error, "Publish error: " + err.ToString());
                    return;
                }

            }


                    
        }

        //bool _askingNewSolution = false;

        

        //public override async void RemovedFromDocument(GH_Document document)
        //{
        //    // disconenct client (ignoring errors)
        //    if (client != null)
        //    {
        //        try
        //        {
        //            await client.DisconnectAsync();
                    

        //        }
        //        catch { }
        //    }

        //    base.RemovedFromDocument(document);
        //}

        public override GH_Exposure Exposure
        {
            get { return GH_Exposure.primary; }
        }


        /// <summary>
        /// Gets the unique ID for this component. Do not change this ID after release.
        /// </summary>
        public override Guid ComponentGuid
        {
            get { return new Guid("61766bfa-7518-4b7f-a66e-cce700900703"); }
        }
    }
}