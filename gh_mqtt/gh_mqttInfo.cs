﻿using System;
using System.Drawing;
using Grasshopper.Kernel;

namespace gh_mqtt
{
    public class gh_mqttInfo : GH_AssemblyInfo
    {
        public override string Name
        {
            get
            {
                return "MQTT";
            }
        }
        public override Bitmap Icon
        {
            get
            {
                //Return a 24x24 pixel bitmap to represent this GHA library.
                return null;
            }
        }
        public override string Description
        {
            get
            {
                //Return a short string describing the purpose of this GHA library.
                return "";
            }
        }
        public override Guid Id
        {
            get
            {
                return new Guid("9447015c-b42e-49ff-8833-55574b6e8d8c");
            }
        }

        public override string AuthorName
        {
            get
            {
                //Return a string identifying you or your company.
                return "";
            }
        }
        public override string AuthorContact
        {
            get
            {
                //Return a string representing your preferred contact details.
                return "";
            }
        }
    }
}
