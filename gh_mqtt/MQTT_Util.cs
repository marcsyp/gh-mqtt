﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Net.Http;
using System.Net.Http.Headers;
using Grasshopper.Kernel;
using Grasshopper.Kernel.Types;
using System.Net.Sockets;
using System.Net;
using MQTTnet;


namespace gh_mqtt
{
    public static class MQTT_Util
    {

        public static bool networkAvailable = false;
        public static string localIP = "unknown";
        public static MQTTnet.Client.IMqttClient mqttClient;

        public static List<string> activeSubscriptions = new List<string>();

        private static string get_version()
        {
            return "0.1.0.0";
        }

        public static void CreateClient()
        {
            var factory = new MqttFactory();
            var mqttClient = factory.CreateMqttClient();


        }

        public static bool CheckDomain()
        {
            string domain = Environment.UserDomainName;

            if (domain == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckForInternet()
        {

            // https://stackoverflow.com/questions/2031824/what-is-the-best-way-to-check-for-internet-connectivity-using-net

            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }
        }


        //public static bool authenticated = false;
        //public static bool networkAvailable = false;
        //// public static List<System.Net.IPAddress> ipAddresses = new List<System.Net.IPAddress>();
        //public static List<Segment.Model.Properties> eventQueue = new List<Segment.Model.Properties>();
        //public static List<Segment.Model.Traits> traitsQueue = new List<Segment.Model.Traits>();
        //public static string localIP = "unknown";

        //public static List<Guid> failedAuthentications = new List<Guid>();

        //public static bool Authenticate(GH_Document gh_doc, string eventName)
        //{

        //    //if (CheckDomain() && CheckForInternet())
        //    if (CheckDomain())
        //    {
        //        authenticated = true;
        //        TrackLoadEvent(gh_doc, eventName);

        //        return true;
        //    }
        //    else
        //    {
        //        TrackLoadEvent(gh_doc, "Unauthorized");
        //        return false;
        //    }

        //}

        //public static bool AuthenticateWithAD(GH_Document gh_doc, string eventName, string userName, string password)
        //{
        //    //bool authentic = false;
        //    //try
        //    //{
        //    //    System.DirectoryServices.DirectoryEntry entry = new System.DirectoryServices.DirectoryEntry("LDAP://nbbj.com", userName, password);
        //    //    object nativeObject = entry.NativeObject;
        //    //    authentic = true;
        //    //}
        //    //catch (System.DirectoryServices.DirectoryServicesCOMException) { }
        //    //return authentic;

        //    var credential = new NetworkCredential(userName, password, "LDAP://nbbj.com");
        //    var server = "LDAP://nbbj.com";
        //    // Use round-robin DNS if it's available.
        //    var ips = Dns.GetHostAddresses(server);
        //    foreach (var ip in ips)
        //    {
        //        using (var ldap = new System.DirectoryServices.Protocols.LdapConnection(server))
        //        {
        //            try
        //            {
        //                ldap.Timeout = TimeSpan.FromSeconds(3);
        //                ldap.Bind(credential);

        //                return true;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw;
        //            }
        //        }
        //    }
        //    return false;
        //}

      

        public static void setIPaddress()
        {
            try
            {

                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
                {
                    socket.Connect("8.8.8.8", 65530);
                    IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                    localIP = endPoint.Address.ToString();

                }
            }
            catch (Exception)
            {


            }
        }

    }
}